Num = 2;
YSymb = Table[Symbol["y" <> ToString[i]], {i, 1, Num}];
PSymb = #' & /@ YSymb;
Y[t_] = #[t] & /@ YSymb;
P[t_] = #[t] & /@ PSymb;
F[t_, y_] := {Log[2 * t + Sqrt[4 * Power[y[[1]],2]]], 
	Sqrt[4 * Power[t, 2] + Power[y[[2]], 2]]}
ifun[t_] = 
 Y[t] /. NDSolve[{P[t] == F[t, Y[t]], Y[0] == {0.25, 1}}, 
    Y[t], {t, 0, 3}][[1]]

SetDirectory[NotebookDirectory[]];
Export["./data/y1.dat", 
  Transpose[Table[{t, ifun[t][[1]]}, {t, 0, 3, 0.1}]], "RawJSON", 
  "Compact" -> True];
Export["./data/y2.dat", 
  Transpose[Table[{t, ifun[t][[2]]}, {t, 0, 3, 0.1}]], "RawJSON", 
  "Compact" -> True];
