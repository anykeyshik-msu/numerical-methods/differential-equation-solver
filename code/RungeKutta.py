from MeshSystem import MeshSystem
from Common import Vector

class MethodSpecs:
    def __init__(self, B, A):
        """Specifies the coefficients to use in the Runge Kutta method
        
        B is the Vector of coefficients b_i used to compute
            y_{n+1} = y_n + h\sum_{i=0}^{s} b_ik_i
        
        A is the list of Vectors of coefficients a_ij used to compute
            k_0 = f(t_n, y_n)
            c_i = \sum_{j=0}^{i}a_{ij}
            k_{i+1} = f(t_n + hc_i, y_n + h\sum_{j=0}^{i}a_{ij}k_j)
        """
        self.B = B
        self.A = A
        self.C = Vector(*(sum(row) for row in A))

EulerMethod = MethodSpecs(Vector(1), [])
MidpointMethod = MethodSpecs(Vector(0,1), [Vector(1/2)])
RungeKuttaMethod = MethodSpecs(Vector(1/6, 1/3, 1/3, 1/6),
        [Vector(1/2),Vector(0,1/2),Vector(0,0,1)])

class RungeKutta(MeshSystem):
    def __init__(self,
            stepT=0.01,
            startT=0,
            startY=Vector(0),
            f=lambda t, y: y,
            method=EulerMethod):
        MeshSystem.__init__(self, stepT=stepT, startT=startT)
        self.A = [x*stepT for x in method.A]
        self.B = method.B*stepT
        self.C = method.C*stepT
        self.f = f
        self[startT] = startY

    def generate(self, request):
        coord = request
        startT = self.startT
        stepT = self.stepT if coord > self.startT else -self.stepT
        elem = self.get(coord)
        while elem is None:
            coord -= stepT
            elem = self.get(coord)
        coord += stepT
        while (abs(startT - coord) < abs(startT - request - stepT/2)):
            self.compute(coord)
            coord += stepT
        return self.get(request)

    def compute(self, coord):
        stepT = self.stepT if coord > self.startT else -self.stepT
        prevT = coord - stepT
        prevY = self[prevT]
        f = self.f
        A = self.A
        B = self.B
        C = self.C
        K = [f(prevT, prevY)]
        for s in range(0, len(B)-1):
            K.append(f(prevT + C[s], prevY + sum(A[s][j]*K[j]
                for j in range(0,len(K)))))
        self[coord] = prevY + sum(K[i]*B[i] for i in range(0, len(K)))
