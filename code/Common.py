from numbers import Number

class Vector(list):
    def __init__(self, *args, **kwargs):
        list.__init__(self, args, **kwargs)

    def __add__(self, other):
        if other == 0:
            return self
        if not isinstance(other, Vector):
            raise TypeError
        if not len(self) == len(other):
            raise ValueError
        return Vector(*[self[i]+other[i] for i in range(0, len(self))])
    __radd__ = __add__

    def __mul__(self, other):
        if not isinstance(other, Number):
            raise TypeError
        return Vector(*[self[i]*other for i in range(0, len(self))])
    __rmul__ = __mul__

    def __truediv__(self, other):
        return self * (1.0/other)
