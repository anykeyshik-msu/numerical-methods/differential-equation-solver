from math import inf

class MeshSystem(dict):
    def __init__(self, stepT=0.01, startT=0.0):
        dict.__init__(self)
        self.stepT = stepT
        self.startT = startT

    def __repr__(self):
        return repr({key2coord(key):val for key,val in self.items()})

    def key2coord(self, key):
        return key*self.stepT + self.startT

    def coord2key(self, coord):
        return int(round((coord-self.startT)/self.stepT))

    def getplotdata(self, left=None, right=None):
        if left is None:
            left = -inf
        else:
            self[left]
        if right is None:
            right = +inf
        else:
            self[right]
        dataKeys = [key for key in self.keys()
                if left <= self.key2coord(key) <= right]
        dataVals = zip(*(self.trueget(key) for key in dataKeys))
        dataT = [self.key2coord(key) for key in dataKeys]
        return [[dataT, dataY] for dataY in dataVals]

    def generate(self, request):
        raise NotImplementedError

    def __missing__(self, request):
        self.generate(request)
        return self.get(request)

    def __setitem__(self, coord, value):
        self.set(coord, value)

    def __getitem__(self, coord):
        val = self.get(coord)
        if val is None:
            return self.__missing__(coord)
        else:
            return val

    def trueget(self, key):
        return super(MeshSystem, self).get(key)

    def trueset(self, key, value):
        return super(MeshSystem, self).__setitem__(key, value)

    def set(self, coord, value):
        return self.trueset(self.coord2key(coord), value)

    def get(self, coord):
        return self.trueget(self.coord2key(coord))
