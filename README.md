# Cauchy problem solver

Second Numerical Methods course task, a Python implementation of a Cauchy problem solver, using the Runge–Kutta methods

### Run project
Run in project dir:
```
$ cd code
$ python3 run1.py
$ python3 run2.py
```
